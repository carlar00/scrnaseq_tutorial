---
title: "scRNAseq_day2"
output:
  html_document: default
  pdf_document: default
date: "2024-06-03"
---

# Annotate cell types

## Step 1. Load library and seurat object

```{r setup, include=FALSE}
library(SingleR)
library(Seurat)
library(celldex)
library(dplyr)
library(pheatmap)

sobj <- readRDS("/home/caruli/Dokumente/Uni/Master/4.Semester/Bioinformatics/carcinoma_folder/bc_tutorial.rds")
```

check seurat object

```{r}
sobj
```

## Step 2. Prepare inputs for running automatic cell type annotation

We need to extract the normalization data using GetAssayData()

```{r}
sobj_count <- GetAssayData(sobj, layer = "data")
str(sobj_count)
```

Get the Blueprint and Encode reference data from celldex package:

```{r}
ref <- celldex::BlueprintEncodeData()
ref
```

## Step 3. Run SingleR to label cell type

```{r}
pred <- SingleR(test = sobj_count, ref = ref, labels = ref$label.main)
pred %>% head()
unique(pred$labels)
```

Next, we put the prediction results back to our seurat object for visualization

```{r}
sobj$labels <- pred[match(rownames(sobj@meta.data), rownames(pred)), 'labels']
sobj@meta.data %>% head()
```

## Step 4. Visualize the data

```{r}
umap_pred <- DimPlot(
  sobj, reduction = 'umap', group.by = 'labels', label = TRUE)

umap_pred
```

Compare with your previous data

```{r}
umap_sobj <- DimPlot(
  sobj, reduction = 'umap', group.by = 'seurat_clusters', label = TRUE)

umap_sobj | umap_pred
```

Plot the SingleR assignment score across all cell-label combination

```{r}
plotScoreHeatmap(pred)
```

Compare x axis with y axis to see if cells are labeled to the same cell type on the x and y axis. For example: first blue box (top left) means astrocytes. When going down the y axis we see that according to the y axis those cells belong to Fibroblasts and Astrocytes.

## Step 5. Marker-based method: CellMarker v2

Load the human markers from CellMarker 2.0, file includes cell markers of different cell types from different tissue in human:

```{r}
human_marker <- read.csv("/home/caruli/Dokumente/Uni/Master/4.Semester/Bioinformatics/carcinoma_folder/CellMakerv2_Human.csv")
str(human_marker)
```

Select marker for ovary from the tissue class column:

```{r}
ovary_marker <- human_marker %>%
  filter(species=="Human" & tissue_class=="Ovary") %>%
  select(c(3,5,6,7,8,9,16))
ovary_marker %>% head()
```

Get markers table from your seurat object

```{r}
sobj.markers <- FindAllMarkers(sobj, only.pos = TRUE)
sobj.markers %>% head()
```

Combine CellMarker with our markers sobj.markers

```{r}
merged_cellmarker <- sobj.markers %>%
  filter(p_val_adj<0.05 & avg_log2FC>1 & pct.1 > 0.8 & pct.2 < 0.5) %>%
  left_join(ovary_marker[,c(3,4,6)], join_by(gene==marker), relationship = "many-to-many")

merged_cellmarker %>% head()
```

Distinguish cluster 2, try to see if there are fibroblasts and astrocytes:

```{r}
merged_cellmarker %>%
  filter(cluster==2 & cell_type=="Cancer cell") %>%
  select(c(7,8,9))
```

From the results, we found PDGFRA, CAV1, and ACTA2 pointing to Fibroblast in cancer cell.

Check again using FeaturePlot()

```{r}
genes <- c("PDGFRA", "CAV1", "ACTA2")
FeaturePlot(sobj, features = genes)
```

## Step 6. Annotate the cell

rename cluster 2 to Fibroblasts using RenameIdents()

```{r}
sobj_new <- RenameIdents(sobj, '2' = 'Fibroblasts')
DimPlot(sobj_new, reduction = 'umap', label = TRUE)
```

## Step 7. Differentially expressed genes (Bonus)

Can we distinguish cluster 7?

```{r}
merged_cellmarker %>%
  filter(cluster==7 & cell_type=="Cancer cell") %>%
  select(c(7,8,9)) %>%
  str()
```

--\> Cluster 7 is either Mesenchymal cells or Adipocytes –\> we need to find out which is the right one

```{r}
# BiocManager::install("clusterProfiler", force=TRUE)
# BiocManager::install("org.Hs.eg.db", force=TRUE)
# BiocManager::install("DOSE", force=TRUE)

library(org.Hs.eg.db)
library(clusterProfiler)
library(DOSE)
```

```{r}
marker_7_2 <- FindMarkers(sobj, ident.1 = 7, ident.2 = 2)
marker_7_2_sig <- marker_7_2 %>%
  filter(abs(avg_log2FC)>=1 & p_val_adj<0.05 & pct.2<0.4)
str(marker_7_2_sig)
```

```{r}
log2 <- marker_7_2_sig$avg_log2FC
names(log2) <- rownames(marker_7_2_sig)

log2_sort <- sort(log2, decreasing = TRUE)
log2_sort[1:5]
```

Run gene set enrichment analysis:

```{r}
gse <- gseGO(geneList = log2_sort,
             ont = "ALL",
             keyType = "SYMBOL",
             nPerm=10000,
             OrgDb = "org.Hs.eg.db")
```

Visualize the results

```{r}
dotplot(gse)
```

--\> Cluster 7 is more likely to be Mesenchymal cells because cluster 7 is more related to immune response and this function fits better to Mesenchymal cells than Adipocytes

Put the identity back:

```{r}
sobj_new <- RenameIdents(sobj,
                         '7' = 'Mesenchymal Cells',
                         '2' = 'Fibroblasts')
DimPlot(sobj_new, reduction = 'umap', label = TRUE)
```
